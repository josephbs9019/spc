<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSreUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sre_users', function (Blueprint $table) {
            $table->increments('id');
	        $table->string('username');
            $table->integer('sre_cat_profile_id')->unsigned();
	        $table->string('password')->nullable()->default(null);
	        $table->boolean('isLdap')->default(1);
            $table->rememberToken();
	        $table->timestamps();
	        $table->softDeletes();

            $table->foreign('sre_cat_profile_id')
                ->references('id')
                ->on('sre_cat_profiles');

            $table->index([
                        'username',
                        'isLdap'
                    ]);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
