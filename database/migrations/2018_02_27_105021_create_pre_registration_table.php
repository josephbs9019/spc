<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePreRegistrationTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pre_registrations', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('person_affected_id')->unsigned()->nullable();
            $table->integer('contact_affected_id')->unsigned()->nullable();
            $table->integer('person_appearing_id')->unsigned()->nullable();
            $table->integer('contact_appearing_id')->unsigned()->nullable();
            $table->integer('sre_cat_office_id')->unsigned()->nullable();
            $table->string('folio',50)->nullable();
            $table->string('eventDescription',2000)->nullable();
            $table->integer('status')->nullable();
            $table->timestamps();
            $table->softDeletes();
            $table->foreign('person_affected_id')->references('id')->on('people');
            $table->foreign('contact_affected_id')->references('id')->on('contacts');
            $table->foreign('sre_cat_office_id')->references('id')->on('sre_cat_offices');
            $table->foreign('person_appearing_id')->references('id')->on('people');
            $table->foreign('contact_appearing_id')->references('id')->on('contacts');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pre_registrations');
    }
}
