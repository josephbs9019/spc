<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePeopleTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('people', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name',250)->nullable();
            $table->string('surname',250)->nullable();
            $table->string('secondSurname',250)->nullable();
            $table->timestamp('birthDate')->nullable();
            $table->tinyInteger('sex')->nullable();
            $table->timestamps();
            $table->softDeletes();
            $table->index([
                        'id',
                        'name'
                    ]);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('people');
    }
}
