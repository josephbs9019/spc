<?php

use Illuminate\Database\Seeder;

class SreEmployeesMinTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {

        \DB::table('sre_employees')->insert(array (
            
            array (
                'id' => 1,
                'name' => 'Juan Manuel',
                'firstName' => 'Rojo',
                'secondName' => 'Márquez',
                'sre_cat_employee_gender_id' => 1,
                'workEmail' => 'jrojo@sre.gob.mx',
                'sre_cat_employee_status_id' => 1,
            ),

            array (
                'id' => 2,
                'name' => 'Marcos',
                'firstName' => 'Piñeiro',
                'secondName' => 'Villegas',
                'sre_cat_employee_gender_id' => 1,
                'workEmail' => 'marcosp@sre.gob.mx',
                'sre_cat_employee_status_id' => 1,
            ),
            
            array (
                'id' => 3,
                'name' => 'Richard Michel',
                'firstName' => 'Atecas',
                'secondName' => 'Nogales',
                'sre_cat_employee_gender_id' => 1,
                'workEmail' => 'ratecas@sre.gob.mx',
                'sre_cat_employee_status_id' => 1,
            ),
            
            array (
                'id' => 4,
                'name' => 'Pedro',
                'firstName' => 'Castillo',
                'secondName' => 'Ramírez',
                'sre_cat_employee_gender_id' => 1,
                'workEmail' => 'pcastillor@sre.gob.mx',
                'sre_cat_employee_status_id' => 1,
            ),
            
            array (
                'id' => 5,
                'name' => 'Homero',
                'firstName' => 'Piedras',
                'secondName' => 'Rodríguez',
                'sre_cat_employee_gender_id' => 1,
                'workEmail' => 'hpiedras@sre.gob.mx',
                'sre_cat_employee_status_id' => 1,
            ),
            
            array (
                'id' => 6,
                'name' => 'Héctor Gustavo',
                'firstName' => 'Aguirre',
                'secondName' => 'Angulo',
                'sre_cat_employee_gender_id' => 1,
                'workEmail' => 'haguirre@sre.gob.mx',
                'sre_cat_employee_status_id' => 1,
            )
        ));
        
        
    }
}