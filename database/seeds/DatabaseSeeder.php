<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		DB::transaction(function () {
			// Date
			$this->call(DateCatMonthsTableSeeder::class);
			// Geo
			$this->call(GeoCatCurrenciesTableSeeder::class);
			$this->call(GeoCatCurrencyExchangeRatesTableSeeder::class);
			$this->call(GeoCatIdiomsTableSeeder::class);
			$this->call(GeoCatContinentsTableSeeder::class);
			$this->call(GeoCatRegionsTableSeeder::class);
			$this->call(GeoCatCountriesTableSeeder::class);
			$this->call(GeoCatStatesTableSeeder::class);
			$this->call(GeoCatMunicipalitiesTableSeeder::class);
			// Ofiices
			$this->call(SreCatOfficeTypesTableSeeder::class);
			$this->call(SreCatOfficesTableSeeder::class);
			$this->call(SreCatAreasTableSeeder::class);
			$this->call(SreCatSubAreasTableSeeder::class);
			$this->call(SreCatDepartmentsTableSeeder::class);
			$this->call(SreCatSubDepartmentsTableSeeder::class);
			// Users
			$this->call(SreCatProfilesTableSeeder::class);
			$this->call(SreUsersTableSeeder::class);
			// Admin forms
			$this->call(AdminFormFieldTypes::class);
		});
	}
}
