<?php

use Illuminate\Database\Seeder;

class GeoCatCurrenciesTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        
        \DB::table('geo_cat_currencies')->insert(array (
            
            array (
                'id' => 1,
                'name' => 'Afgani',
                'symbol' => '؋',
                'iso3' => 'AFN'
            ),
            
            array (
                'id' => 2,
                'name' => 'Ariari',
                'symbol' => 'Ar',
                'iso3' => 'MGA'
            ),
            
            array (
                'id' => 3,
                'name' => 'Balboa Panameño',
                'symbol' => 'B/.',
                'iso3' => 'PAB'
            ),
            
            array (
                'id' => 4,
                'name' => 'Bat',
                'symbol' => '฿',
                'iso3' => 'THB'
            ),
            
            array (
                'id' => 5,
                'name' => 'Bir',
                'symbol' => 'Br',
                'iso3' => 'ETB'
            ),
            
            array (
                'id' => 6,
                'name' => 'Bolívar Fuerte',
                'symbol' => 'Bs F',
                'iso3' => 'VEF'
            ),
            
            array (
                'id' => 7,
                'name' => 'Boliviano',
                'symbol' => 'Bs.',
                'iso3' => 'BOB'
            ),
            
            array (
                'id' => 8,
                'name' => 'Cedi de Gana',
                'symbol' => '₵',
                'iso3' => 'GHS'
            ),
            
            array (
                'id' => 9,
                'name' => 'Chelín Keniano',
                'symbol' => 'Sh',
                'iso3' => 'KES'
            ),
            
            array (
                'id' => 10,
                'name' => 'Chelín Somalí',
                'symbol' => 'Sh',
                'iso3' => 'SOS'
            ),
            
            array (
                'id' => 11,
                'name' => 'Chelín Tanzaniano',
                'symbol' => 'Sh',
                'iso3' => 'TZS'
            ),
            
            array (
                'id' => 12,
                'name' => 'Chelín Ugandés',
                'symbol' => 'Sh',
                'iso3' => 'UGX'
            ),
            
            array (
                'id' => 13,
                'name' => 'Colón Costarricense',
                'symbol' => '₡',
                'iso3' => 'CRC'
            ),
            
            array (
                'id' => 14,
                'name' => 'Colón Salvadoreño',
                'symbol' => '₡',
                'iso3' => 'SVC'
            ),
            
            array (
                'id' => 15,
                'name' => 'Córdoba Nicaragüense',
                'symbol' => 'C$',
                'iso3' => 'NIO'
            ),
            
            array (
                'id' => 16,
                'name' => 'Corona Checa',
                'symbol' => 'Kč',
                'iso3' => 'CZK'
            ),
            
            array (
                'id' => 17,
                'name' => 'Corona Danesa',
                'symbol' => 'kr',
                'iso3' => 'DKK'
            ),
            
            array (
                'id' => 18,
                'name' => 'Corona Islandesa',
                'symbol' => 'kr',
                'iso3' => 'ISK'
            ),
            
            array (
                'id' => 19,
                'name' => 'Corona Noruega',
                'symbol' => 'kr',
                'iso3' => 'NOK'
            ),
            
            array (
                'id' => 20,
                'name' => 'Corona Sueca',
                'symbol' => 'kr',
                'iso3' => 'SEK'
            ),
            
            array (
                'id' => 21,
                'name' => 'Dalasi Gambiano',
                'symbol' => 'D',
                'iso3' => 'GMD'
            ),
            
            array (
                'id' => 22,
                'name' => 'Denar',
                'symbol' => 'ден',
                'iso3' => 'MKD'
            ),
            
            array (
                'id' => 23,
                'name' => 'Dinar',
                'symbol' => 'د.ج',
                'iso3' => 'DZD'
            ),
            
            array (
                'id' => 24,
                'name' => 'Dinar de Baréin',
                'symbol' => '.د.ب',
                'iso3' => 'BHD'
            ),
            
            array (
                'id' => 25,
                'name' => 'Dinar Irakí',
                'symbol' => 'ع.د',
                'iso3' => 'IQD'
            ),
            
            array (
                'id' => 26,
                'name' => 'Dinar Jordano',
                'symbol' => 'د.ا',
                'iso3' => 'JOD'
            ),
            
            array (
                'id' => 27,
                'name' => 'Dinar Kuwaití',
                'symbol' => 'د.ك',
                'iso3' => 'KWD'
            ),
            
            array (
                'id' => 28,
                'name' => 'Dinar Libio',
                'symbol' => 'ل.د',
                'iso3' => 'LYD'
            ),
            
            array (
                'id' => 29,
                'name' => 'Dinar Serbio',
                'symbol' => 'дин. o din.',
                'iso3' => 'RSD'
            ),
            
            array (
                'id' => 30,
                'name' => 'Dinar Tunecino',
                'symbol' => 'د.ت',
                'iso3' => 'TND'
            ),
            
            array (
                'id' => 31,
                'name' => 'Dírham',
                'symbol' => 'د.إ',
                'iso3' => 'AED'
            ),
            
            array (
                'id' => 32,
                'name' => 'Dobra',
                'symbol' => 'Db',
                'iso3' => 'STD'
            ),
            
            array (
                'id' => 33,
                'name' => 'Dólar Australiano',
                'symbol' => '$',
                'iso3' => 'AUD'
            ),
            
            array (
                'id' => 34,
                'name' => 'Dólar Beliceño',
                'symbol' => '$',
                'iso3' => 'BZD'
            ),
            
            array (
                'id' => 35,
                'name' => 'Dólar Canadiense',
                'symbol' => '$',
                'iso3' => 'CAD'
            ),
            
            array (
                'id' => 36,
                'name' => 'Dólar de Brunei',
                'symbol' => '$',
                'iso3' => 'BND'
            ),
            
            array (
                'id' => 37,
                'name' => 'Dólar de las Bahamas',
                'symbol' => '$',
                'iso3' => 'BSD'
            ),
            
            array (
                'id' => 38,
                'name' => 'Dólar de las Barbados',
                'symbol' => '$',
                'iso3' => 'BBD'
            ),
            
            array (
                'id' => 39,
                'name' => 'Dólar de las Islas Caimán',
                'symbol' => '$',
                'iso3' => 'KYD'
            ),
            
            array (
                'id' => 40,
                'name' => 'Dólar de Namibia',
                'symbol' => '$',
                'iso3' => 'NAD'
            ),
            
            array (
                'id' => 41,
                'name' => 'Dólar de Nueva Zelanda',
                'symbol' => '$',
                'iso3' => 'NZD'
            ),
            
            array (
                'id' => 42,
                'name' => 'Dólar del Caribe Oriental',
                'symbol' => '$',
                'iso3' => 'XCD'
            ),
            
            array (
                'id' => 43,
                'name' => 'Dólar Americano',
                'symbol' => '$',
                'iso3' => 'USD'
            ),
            
            array (
                'id' => 44,
                'name' => 'Dólar Jamaicano',
                'symbol' => '$',
                'iso3' => 'JMD'
            ),
            
            array (
                'id' => 45,
                'name' => 'Dólar Liberiano',
                'symbol' => '$',
                'iso3' => 'LRD'
            ),
            
            array (
                'id' => 46,
                'name' => 'Dólar Micronesio',
                'symbol' => '$',
                'iso3' => 'nenhum'
            ),
            
            array (
                'id' => 47,
                'name' => 'Dólar Nauruano',
                'symbol' => '$',
                'iso3' => 'Nenhum'
            ),
            
            array (
                'id' => 48,
                'name' => 'Dólar Salomonense',
                'symbol' => '$',
                'iso3' => 'SBD'
            ),
            
            array (
                'id' => 49,
                'name' => 'Dólar Singapurense',
                'symbol' => '$',
                'iso3' => 'SGD'
            ),
            
            array (
                'id' => 50,
                'name' => 'Dólar Surinamés',
                'symbol' => '$',
                'iso3' => 'SRD'
            ),
            
            array (
                'id' => 51,
                'name' => 'Dólar Trinitense',
                'symbol' => '$',
                'iso3' => 'TTD'
            ),
            
            array (
                'id' => 52,
                'name' => 'Dólar Zimbabuense',
                'symbol' => '$',
                'iso3' => 'ZWL'
            ),
            
            array (
                'id' => 53,
                'name' => 'Dong',
                'symbol' => '₫',
                'iso3' => 'VND'
            ),
            
            array (
                'id' => 54,
                'name' => 'Dram Armenio',
                'symbol' => NULL,
                'iso3' => 'AMD'
            ),
            
            array (
                'id' => 55,
                'name' => 'Escudo de Cabo Verde',
                'symbol' => 'Esc, $',
                'iso3' => 'CVE'
            ),
            
            array (
                'id' => 56,
                'name' => 'Euro',
                'symbol' => '€',
                'iso3' => 'EUR'
            ),
            
            array (
                'id' => 57,
                'name' => 'Florín Arubano',
                'symbol' => 'ƒ',
                'iso3' => 'AWG'
            ),
            
            array (
                'id' => 58,
                'name' => 'Florín de las Antillas Neerlandesas',
                'symbol' => 'ƒ',
                'iso3' => 'ANG'
            ),
            
            array (
                'id' => 59,
                'name' => 'Forinto',
                'symbol' => 'Ft',
                'iso3' => 'HUF'
            ),
            
            array (
                'id' => 60,
                'name' => 'Franco Burundés',
                'symbol' => 'Fr',
                'iso3' => 'BIF'
            ),
            
            array (
                'id' => 61,
                'name' => 'Franco CFA',
                'symbol' => 'Fr',
                'iso3' => 'XOF'
            ),
            
            array (
                'id' => 62,
                'name' => 'Franco Comorano',
                'symbol' => 'Fr',
                'iso3' => 'KMF'
            ),
            
            array (
                'id' => 63,
                'name' => 'Franco Congoleño',
                'symbol' => 'Fr',
                'iso3' => 'CDF'
            ),
            
            array (
                'id' => 64,
                'name' => 'Franco Guineano',
                'symbol' => 'Fr',
                'iso3' => 'GNF'
            ),
            
            array (
                'id' => 65,
                'name' => 'Franco Ruandés',
                'symbol' => 'Fr',
                'iso3' => 'RWF'
            ),
            
            array (
                'id' => 66,
                'name' => 'Franco Suizo',
                'symbol' => 'Fr',
                'iso3' => 'CHF'
            ),
            
            array (
                'id' => 67,
                'name' => 'Franco Yibutiano',
                'symbol' => 'Fr',
                'iso3' => 'DJF'
            ),
            
            array (
                'id' => 68,
                'name' => 'Grivna',
                'symbol' => '₴',
                'iso3' => 'UAH'
            ),
            
            array (
                'id' => 69,
                'name' => 'Guaraní Paraguayo',
                'symbol' => '₲',
                'iso3' => 'PYG'
            ),
            
            array (
                'id' => 70,
                'name' => 'Gultrum Butanés',
                'symbol' => 'Nu.',
                'iso3' => 'BTN'
            ),
            
            array (
                'id' => 71,
                'name' => 'Gurde',
                'symbol' => 'G',
                'iso3' => 'HTG'
            ),
            
            array (
                'id' => 72,
                'name' => 'Kiat de Birmania',
                'symbol' => 'K',
                'iso3' => 'MMK'
            ),
            
            array (
                'id' => 73,
                'name' => 'Kina',
                'symbol' => 'K',
                'iso3' => 'PGK'
            ),
            
            array (
                'id' => 74,
                'name' => 'Kip',
                'symbol' => '₭',
                'iso3' => 'LAK'
            ),
            
            array (
                'id' => 75,
                'name' => 'Kuacha de Malaui',
                'symbol' => 'MK',
                'iso3' => 'MWK'
            ),
            
            array (
                'id' => 76,
                'name' => 'Kuacha Zambiano',
                'symbol' => 'ZK',
                'iso3' => 'ZMK'
            ),
            
            array (
                'id' => 77,
                'name' => 'Kuanza de Angola',
                'symbol' => 'Kz',
                'iso3' => 'AOA'
            ),
            
            array (
                'id' => 78,
                'name' => 'Kuna Croata',
                'symbol' => 'kn',
                'iso3' => 'HRK'
            ),
            
            array (
                'id' => 79,
                'name' => 'Lari Georgiano',
                'symbol' => 'ლ',
                'iso3' => 'GEL'
            ),
            
            array (
                'id' => 80,
                'name' => 'Lats',
                'symbol' => 'Ls',
                'iso3' => 'LVL'
            ),
            
            array (
                'id' => 81,
                'name' => 'Lek',
                'symbol' => 'L',
                'iso3' => 'ALL'
            ),
            
            array (
                'id' => 82,
                'name' => 'Lempira',
                'symbol' => 'L',
                'iso3' => 'HNL'
            ),
            
            array (
                'id' => 83,
                'name' => 'Leona',
                'symbol' => 'Le',
                'iso3' => 'SLL'
            ),
            
            array (
                'id' => 84,
                'name' => 'Leu Moldavo',
                'symbol' => 'L',
                'iso3' => 'MDL'
            ),
            
            array (
                'id' => 85,
                'name' => 'Leu Rumano',
                'symbol' => 'L',
                'iso3' => 'RON'
            ),
            
            array (
                'id' => 86,
                'name' => 'Leva Búlgaro',
                'symbol' => 'лв',
                'iso3' => 'BGN'
            ),
            
            array (
                'id' => 87,
                'name' => 'Libra de Gibraltar',
                'symbol' => '£',
                'iso3' => 'GIP'
            ),
            
            array (
                'id' => 88,
                'name' => 'Libra Egipcia',
                'symbol' => '£, ج.م',
                'iso3' => 'EGP'
            ),
            
            array (
                'id' => 89,
                'name' => 'Libra Esterlina',
                'symbol' => '£',
                'iso3' => 'GBP'
            ),
            
            array (
                'id' => 90,
                'name' => 'Libra Libanesa',
                'symbol' => 'ل.ل',
                'iso3' => 'LBP'
            ),
            
            array (
                'id' => 91,
                'name' => 'Libra Siria',
                'symbol' => '£ o ل.س',
                'iso3' => 'SYP'
            ),
            
            array (
                'id' => 92,
                'name' => 'Libra Sudanesa',
                'symbol' => '£',
                'iso3' => 'SDG'
            ),
            
            array (
                'id' => 93,
                'name' => 'Libra Sursudanesa',
                'symbol' => '£',
                'iso3' => 'SSP'
            ),
            
            array (
                'id' => 94,
                'name' => 'Lilangeni',
                'symbol' => 'L',
                'iso3' => 'SZL'
            ),
            
            array (
                'id' => 95,
                'name' => 'Lira Turca',
                'symbol' => NULL,
                'iso3' => 'TRY'
            ),
            
            array (
                'id' => 96,
                'name' => 'Litas',
                'symbol' => 'Lt',
                'iso3' => 'LTL'
            ),
            
            array (
                'id' => 97,
                'name' => 'Loti de Lesoto',
                'symbol' => 'L',
                'iso3' => 'LSL'
            ),
            
            array (
                'id' => 98,
                'name' => 'Manat Azerbayano',
                'symbol' => NULL,
                'iso3' => 'AZN'
            ),
            
            array (
                'id' => 99,
                'name' => 'Manat Turcomano',
                'symbol' => 'm',
                'iso3' => 'TMT'
            ),
            
            array (
                'id' => 100,
                'name' => 'Marco Convertible de Bosnia y Herzegovina',
                'symbol' => 'KM',
                'iso3' => 'BAM'
            ),
            
            array (
                'id' => 101,
                'name' => 'Metical',
                'symbol' => 'MT',
                'iso3' => 'MZN'
            ),
            
            array (
                'id' => 102,
                'name' => 'Naira Nigeriano',
                'symbol' => '₦',
                'iso3' => 'NGN'
            ),
            
            array (
                'id' => 103,
                'name' => 'Nakfa de Eritrea',
                'symbol' => 'Nfk',
                'iso3' => 'ERN'
            ),
            
            array (
                'id' => 104,
                'name' => 'Nuevo Dólar de Taiwán',
                'symbol' => '$',
                'iso3' => 'TWD'
            ),
            
            array (
                'id' => 105,
                'name' => 'Nuevo Séquel',
                'symbol' => '₪',
                'iso3' => 'ILS'
            ),
            
            array (
                'id' => 106,
                'name' => 'Nuevo Sol Peruano',
                'symbol' => 'S/.',
                'iso3' => 'PEN'
            ),
            
            array (
                'id' => 107,
                'name' => 'Paanga',
                'symbol' => '$',
                'iso3' => 'TOP'
            ),
            
            array (
                'id' => 108,
                'name' => 'Pataca de Macao',
                'symbol' => 'P',
                'iso3' => 'MOP'
            ),
            
            array (
                'id' => 109,
                'name' => 'Peso Argentino',
                'symbol' => '$',
                'iso3' => 'ARS'
            ),
            
            array (
                'id' => 110,
                'name' => 'Peso Chileno',
                'symbol' => '$',
                'iso3' => 'CLP'
            ),
            
            array (
                'id' => 111,
                'name' => 'Peso Colombiano',
                'symbol' => '$',
                'iso3' => 'COP'
            ),
            
            array (
                'id' => 112,
                'name' => 'Peso Cubano',
                'symbol' => '$',
                'iso3' => 'CUP'
            ),
            
            array (
                'id' => 113,
                'name' => 'Peso Dominicano',
                'symbol' => '$',
                'iso3' => 'DOP'
            ),
            
            array (
                'id' => 114,
                'name' => 'Peso Filipino',
                'symbol' => '₱',
                'iso3' => 'PHP'
            ),
            
            array (
                'id' => 115,
                'name' => 'Peso Mexicano',
                'symbol' => '$',
                'iso3' => 'MXN'
            ),
            
            array (
                'id' => 116,
                'name' => 'Peso Uruguayo',
                'symbol' => '$',
                'iso3' => 'UYU'
            ),
            
            array (
                'id' => 117,
                'name' => 'Pula de Botsuana',
                'symbol' => 'P',
                'iso3' => 'BWP'
            ),
            
            array (
                'id' => 118,
                'name' => 'Quetzal',
                'symbol' => 'Q',
                'iso3' => 'GTQ'
            ),
            
            array (
                'id' => 119,
                'name' => 'Rand',
                'symbol' => 'R',
                'iso3' => 'ZAR'
            ),
            
            array (
                'id' => 120,
                'name' => 'Real Brasileño',
                'symbol' => 'R$',
                'iso3' => 'BRL'
            ),
            
            array (
                'id' => 121,
                'name' => 'Rial Catarí',
                'symbol' => 'ر.ق',
                'iso3' => 'QAR'
            ),
            
            array (
                'id' => 122,
                'name' => 'Rial de Omán',
                'symbol' => 'ر.ع.',
                'iso3' => 'OMR'
            ),
            
            array (
                'id' => 123,
                'name' => 'Rial Iraní',
                'symbol' => NULL,
                'iso3' => 'IRR'
            ),
            
            array (
                'id' => 124,
                'name' => 'Rial Saudí',
                'symbol' => 'ر.س',
                'iso3' => 'SAR'
            ),
            
            array (
                'id' => 125,
                'name' => 'Rial Yemení',
                'symbol' => '﷼',
                'iso3' => 'YER'
            ),
            
            array (
                'id' => 126,
                'name' => 'Riel Camboyano',
                'symbol' => '៛',
                'iso3' => 'KHR'
            ),
            
            array (
                'id' => 127,
                'name' => 'Ringit',
                'symbol' => 'RM',
                'iso3' => 'MYR'
            ),
            
            array (
                'id' => 128,
                'name' => 'Rublo',
                'symbol' => '₽',
                'iso3' => 'RUB'
            ),
            
            array (
                'id' => 129,
                'name' => 'Rublo Bielorruso',
                'symbol' => 'Br',
                'iso3' => 'BYR'
            ),
            
            array (
                'id' => 130,
                'name' => 'Rufiya',
                'symbol' => '.ރ',
                'iso3' => 'MVR'
            ),
            
            array (
                'id' => 131,
                'name' => 'Rupia Ceilandesa',
                'symbol' => 'Rs',
                'iso3' => 'LKR'
            ),
            
            array (
                'id' => 132,
                'name' => 'Rupia de Mauricio',
                'symbol' => '₨',
                'iso3' => 'MUR'
            ),
            
            array (
                'id' => 133,
                'name' => 'Rupia India',
                'symbol' => '₹',
                'iso3' => 'INR'
            ),
            
            array (
                'id' => 134,
                'name' => 'Rupia Indonesia',
                'symbol' => 'Rp',
                'iso3' => 'IDR'
            ),
            
            array (
                'id' => 135,
                'name' => 'Rupia Nepalí',
                'symbol' => '₨',
                'iso3' => 'NPR'
            ),
            
            array (
                'id' => 136,
                'name' => 'Rupia Pakistaní',
                'symbol' => '₨',
                'iso3' => 'PKR'
            ),
            
            array (
                'id' => 137,
                'name' => 'Rupia Seychellense',
                'symbol' => '₨',
                'iso3' => 'SCR'
            ),
            
            array (
                'id' => 138,
                'name' => 'Som Kirguís',
                'symbol' => 'лв',
                'iso3' => 'KGS'
            ),
            
            array (
                'id' => 139,
                'name' => 'Somoni',
                'symbol' => 'ЅМ',
                'iso3' => 'TJS'
            ),
            
            array (
                'id' => 140,
                'name' => 'Sum',
                'symbol' => 'лв',
                'iso3' => 'UZS'
            ),
            
            array (
                'id' => 141,
                'name' => 'Taka de Bangladés',
                'symbol' => '৳',
                'iso3' => 'BDT'
            ),
            
            array (
                'id' => 142,
                'name' => 'Tala',
                'symbol' => 'T',
                'iso3' => 'WST'
            ),
            
            array (
                'id' => 143,
                'name' => 'Tengue Kazajo',
                'symbol' => '₸',
                'iso3' => 'KZT'
            ),
            
            array (
                'id' => 144,
                'name' => 'Tugrik Mongol',
                'symbol' => '₮',
                'iso3' => 'MNT'
            ),
            
            array (
                'id' => 145,
                'name' => 'Uguiya',
                'symbol' => 'UM',
                'iso3' => 'MRO'
            ),
            
            array (
                'id' => 146,
                'name' => 'Vatu do Vanuatu',
                'symbol' => 'Vt',
                'iso3' => 'VUV'
            ),
            
            array (
                'id' => 147,
                'name' => 'Won Norcoreano',
                'symbol' => '₩',
                'iso3' => 'KPW'
            ),
            
            array (
                'id' => 148,
                'name' => 'Won Surcoreano',
                'symbol' => '₩',
                'iso3' => 'KRW'
            ),
            
            array (
                'id' => 149,
                'name' => 'Yen',
                'symbol' => '¥',
                'iso3' => 'JPY'
            ),
            
            array (
                'id' => 150,
                'name' => 'Yuan, Renminbi',
                'symbol' => '¥, 元',
                'iso3' => 'CNY'
            ),
            
            array (
                'id' => 151,
                'name' => 'Złóti',
                'symbol' => 'zł',
                'iso3' => 'PLN'
            ),

            array (
                'id' => 152,
                'name' => 'Bolívar Venezolano',
                'symbol' => NULL,
                'iso3' => 'VEB'
            ),

            array (
                'id' => 153,
                'name' => 'Dirhan Marroqui',
                'symbol' => NULL,
                'iso3' => 'MAD'
            ),

            array (
                'id' => 154,
                'name' => 'Dólar Guyanes',
                'symbol' => NULL,
                'iso3' => 'GYD'
            ),

            array (
                'id' => 155,
                'name' => 'Dólar Guyanes',
                'symbol' => NULL,
                'iso3' => 'GYD'
            ),

            array (
                'id' => 156,
                'name' => 'Dólar Hong Kong',
                'symbol' => NULL,
                'iso3' => 'HKD'
            ),

            array (
                'id' => 157,
                'name' => 'Leu Romano',
                'symbol' => NULL,
                'iso3' => 'ROL'
            ),

            array (
                'id' => 158,
                'name' => 'Liras Turcas',
                'symbol' => NULL,
                'iso3' => 'TRL'
            ),

            array (
                'id' => 159,
                'name' => 'Nuevo Kwanza',
                'symbol' => NULL,
                'iso3' => 'AON'
            ),

            array (
                'id' => 160,
                'name' => 'Peso Cubano Convertible',
                'symbol' => NULL,
                'iso3' => 'CUC'
            ),

        ));
        
        
    }
}