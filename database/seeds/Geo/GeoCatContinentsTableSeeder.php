<?php

use Illuminate\Database\Seeder;

class GeoCatContinentsTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {

        \DB::table('geo_cat_continents')->insert(array (
            
            array (
                'id' => 1,
                'name' => 'África',
                'created_at' => '2018-01-14 04:03:09',
                'updated_at' => '2018-01-14 04:03:09',
                'deleted_at' => NULL,
            ),
            
            array (
                'id' => 2,
                'name' => 'América',
                'created_at' => '2018-01-14 04:03:09',
                'updated_at' => '2018-01-14 04:03:09',
                'deleted_at' => NULL,
            ),
            
            array (
                'id' => 3,
                'name' => 'Antártida',
                'created_at' => '2018-01-14 04:03:09',
                'updated_at' => '2018-01-14 04:03:09',
                'deleted_at' => NULL,
            ),
            
            array (
                'id' => 4,
                'name' => 'Asia',
                'created_at' => '2018-01-14 04:03:09',
                'updated_at' => '2018-01-14 04:03:09',
                'deleted_at' => NULL,
            ),
            
            array (
                'id' => 5,
                'name' => 'Europa',
                'created_at' => '2018-01-14 04:03:09',
                'updated_at' => '2018-01-14 04:03:09',
                'deleted_at' => NULL,
            ),
            
            array (
                'id' => 6,
                'name' => 'Oceanía',
                'created_at' => '2018-01-14 04:03:09',
                'updated_at' => '2018-01-14 04:03:09',
                'deleted_at' => NULL,
            ),
        ));
        
        
    }
}