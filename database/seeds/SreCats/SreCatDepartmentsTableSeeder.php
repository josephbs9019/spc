<?php

use Illuminate\Database\Seeder;

class SreCatDepartmentsTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        
        \DB::table('sre_cat_departments')->insert(array (
            
            array (
                'id' => 4,
                'name' => 'Comisión de Personal del Servicio Exterior Mexicano',
                'acronym' => NULL,
                'sre_cat_area_id' => 1
            ),
            
            array (
                'id' => 5,
                'name' => 'Coordinación General de Asesores',
                'acronym' => NULL,
                'sre_cat_area_id' => 1
            ),
            
            array (
                'id' => 6,
                'name' => 'Centro de Enlace Diplomático',
                'acronym' => NULL,
                'sre_cat_area_id' => 1
            ),
            
            array (
                'id' => 7,
                'name' => 'Dirección General de Protocolo',
                'acronym' => NULL,
                'sre_cat_area_id' => 1
            ),
            
            array (
                'id' => 8,
                'name' => 'Dirección General de Coordinación Política',
                'acronym' => NULL,
                'sre_cat_area_id' => 1
            ),
            
            array (
                'id' => 9,
                'name' => 'Dirección General de Comunicación Social',
                'acronym' => NULL,
                'sre_cat_area_id' => 1
            ),
            
            array (
                'id' => 10,
                'name' => 'Dirección General de Asuntos Jurídicos',
                'acronym' => NULL,
                'sre_cat_area_id' => 1
            ),
            
            array (
                'id' => 11,
                'name' => 'Instituto Matías Romero',
                'acronym' => 'IMR',
                'sre_cat_area_id' => 1
            ),
            
            array (
                'id' => 12,
                'name' => 'Dirección General para Asia-Pacífico',
                'acronym' => NULL,
                'sre_cat_area_id' => 2
            ),
            
            array (
                'id' => 13,
                'name' => 'Dirección General para África y Medio Oriente',
                'acronym' => NULL,
                'sre_cat_area_id' => 2
            ),
            
            array (
                'id' => 14,
                'name' => 'Dirección General para Europa',
                'acronym' => NULL,
                'sre_cat_area_id' => 2
            ),
            
            array (
                'id' => 15,
                'name' => 'Dirección General para América del Norte',
                'acronym' => NULL,
                'sre_cat_area_id' => 3
            ),
            
            array (
                'id' => 16,
                'name' => 'Dirección General de Protección de Mexicanos en el Exterior',
                'acronym' => NULL,
                'sre_cat_area_id' => 3
            ),
            
            array (
                'id' => 17,
                'name' => 'Dirección General de Servicios Consulares',
                'acronym' => NULL,
                'sre_cat_area_id' => 3
            ),
            
            array (
                'id' => 18,
                'name' => 'Instituto de los Mexicanos en el Exterior',
                'acronym' => 'IME',
                'sre_cat_area_id' => 3
            ),
            
            array (
                'id' => 19,
                'name' => 'Seccion Mexicana de la Comisión Internacional de Límites y Aguas entre México-EE.UU.',
                'acronym' => NULL,
                'sre_cat_area_id' => 3
            ),
            
            array (
                'id' => 20,
                'name' => 'Dirección General de Asuntos Especiales',
                'acronym' => NULL,
                'sre_cat_area_id' => 3
            ),
            
            array (
                'id' => 21,
                'name' => 'Dirección General para América Latina y el Caribe',
                'acronym' => NULL,
                'sre_cat_area_id' => 4
            ),
            
            array (
                'id' => 22,
                'name' => 'Direción General de Organismos y Mecanismos Regionales Americanos',
                'acronym' => NULL,
                'sre_cat_area_id' => 4
            ),
            
            array (
                'id' => 23,
                'name' => 'Sección Mexicana de la Comisión Internacional de Límites y Aguas entre México-Guatemala, y entre México y Belice',
                'acronym' => NULL,
                'sre_cat_area_id' => 4
            ),
            
            array (
                'id' => 24,
                'name' => 'Dirección General para Temas Globales',
                'acronym' => NULL,
                'sre_cat_area_id' => 5
            ),
            
            array (
                'id' => 25,
                'name' => 'Dirección General de Derechos Humanos y Democracia',
                'acronym' => NULL,
                'sre_cat_area_id' => 5
            ),
            
            array (
                'id' => 26,
                'name' => 'Dirección General para la Organización de las Naciones Unidas',
                'acronym' => NULL,
                'sre_cat_area_id' => 5
            ),
            
            array (
                'id' => 27,
                'name' => 'Dirección General de Vinculación con las Organizaciones de la Sociedad Civil',
                'acronym' => NULL,
                'sre_cat_area_id' => 5
            ),
            
            array (
                'id' => 28,
                'name' => 'Dirección General del Servicio Exterior y de Recursos Humanos',
                'acronym' => 'DGSERH',
                'sre_cat_area_id' => 6
            ),
            
            array (
                'id' => 29,
                'name' => 'Dirección General de Programación, Organización y Presupuesto',
                'acronym' => 'DGPOP',
                'sre_cat_area_id' => 6
            ),
            
            array (
                'id' => 30,
                'name' => 'Dirección General de Tecnologías de Información e Innovación',
                'acronym' => 'DGTII',
                'sre_cat_area_id' => 6
            ),
            
            array (
                'id' => 31,
                'name' => 'Dirección General de Delegaciones',
                'acronym' => 'DGD',
                'sre_cat_area_id' => 6
            ),
            
            array (
                'id' => 32,
                'name' => 'Dirección General de Bienes Inmuebles y Recursos Materiales',
                'acronym' => 'DGBIRM',
                'sre_cat_area_id' => 6
            ),
            
            array (
                'id' => 33,
                'name' => 'Dirección General de Cooperación Educativa y Cultural',
                'acronym' => NULL,
                'sre_cat_area_id' => 7
            ),
            
            array (
                'id' => 34,
                'name' => 'Dirección General de Cooperación y Promoción Económica Internacional',
                'acronym' => NULL,
                'sre_cat_area_id' => 7
            ),
            
            array (
                'id' => 35,
                'name' => 'Dirección General de Cooperación y Relaciones Economómicas Bilaterales',
                'acronym' => NULL,
                'sre_cat_area_id' => 7
            ),
            
            array (
                'id' => 36,
                'name' => 'Dirección General de Cooperación Técnica y Científica',
                'acronym' => NULL,
                'sre_cat_area_id' => 7
            ),
            
            array (
                'id' => 37,
                'name' => 'Direacción General del Proyecto de Integración y Desarrollo de Mesoamérica',
                'acronym' => NULL,
                'sre_cat_area_id' => 7
            ),
            
            array (
                'id' => 38,
                'name' => 'Dirección General del Acervo Histórico Diplomático',
                'acronym' => NULL,
                'sre_cat_area_id' => 8
            ),
            
            array (
                'id' => 39,
                'name' => 'Auditoría Interna',
                'acronym' => NULL,
                'sre_cat_area_id' => 9
            ),
            
            array (
                'id' => 40,
                'name' => 'Auditoria para el Desarrollo y Mejora de la Gestión Pública',
                'acronym' => NULL,
                'sre_cat_area_id' => 9
            ),
            
            array (
                'id' => 41,
                'name' => 'Responsabilidades',
                'acronym' => NULL,
                'sre_cat_area_id' => 9
            ),
            
            array (
                'id' => 42,
                'name' => 'Quejas',
                'acronym' => NULL,
                'sre_cat_area_id' => 9
            ),
        ));
        
        
    }
}