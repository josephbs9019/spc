<?php

use Illuminate\Database\Seeder;

class SreCatEmployeeStatusesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
	    DB::table('sre_cat_employee_statuses')->insert(
	    	[
			    [ 'name' => 'Activo' ],
			    [ 'name' => 'Inactivo' ]
		    ]
	    );
    }
}
