<?php

use Illuminate\Database\Seeder;

class SreCatEducationLevelsTableSeeder extends Seeder
{
	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		DB::table('sre_cat_education_levels')->insert(
			[
				[ 'name' => 'Primaria' ],
				[ 'name' => 'Secundaria' ],
				[ 'name' => 'Preparatoria' ],
				[ 'name' => 'Licenciatura' ],
				[ 'name' => 'Maestría' ],
				[ 'name' => 'Doctorado' ]
			]
		);
	}
}
