<?php

use Illuminate\Database\Seeder;

class SreCatDependantTypesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
	    DB::table('sre_cat_dependant_types')->insert(
	    	[
			    ['name' => 'Hijo(a)'],
			    ['name' => 'Esposo(a)'],
				['name' => 'Padre'],
			    ['name' => 'Madre'],
				['name'	=> 'Visa']
		    ]
	    );
    }
}
