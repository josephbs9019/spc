<?php

use Illuminate\Database\Seeder;

class SreCatEmployeeMaritalStatusesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
	    DB::table('sre_cat_employee_marital_statuses')->insert(
	    	[
			    [ 'name' => 'Soltero(a)' ],
			    [ 'name' => 'Casado(a)' ],
			    [ 'name' => 'Divorciado(a)' ],
			    [ 'name' => 'Viudo(a)' ]
		    ]
	    );
    }
}
