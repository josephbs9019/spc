import { RouterView } from './RouterView.js';
import GeneralBulkLoad from '../components/views/bulkloads/GeneralBulkLoad';
import RepatriationBulkLoad from '../components/views/bulkloads/RepatriationBulkLoad';
import FameuBulkLoad from '../components/views/bulkloads/FameuBulkLoad';

export default {
		path: 'cargas',
		component: RouterView,
		children: [
			{
				path: 'general',
				component: GeneralBulkLoad
			},
			{
				path: 'repatriacion',
				component: RepatriationBulkLoad
			},
			{
				path: 'fameu',
				component: FameuBulkLoad
			}
		]
}