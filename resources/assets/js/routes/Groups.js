import { RouterView } from './RouterView.js';
import GroupSearch from '../components/views/groups/GroupSearch';

export default {
		path: 'grupales',
		component: RouterView,
		children: [
			{
				path: 'consulta',
				component: GroupSearch
			}
		]
}