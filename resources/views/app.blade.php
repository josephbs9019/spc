<!doctype html>
<html lang="{{ app()->getLocale() }}">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<!-- Favicon -->
		<link rel="shortcut icon" href="{{ asset('img/favicon/favicon.ico') }}" />
		<title>SPC</title>
		<!-- CSS -->
		<link href="{{ asset('css/style.css') }}">
	</head>
	<body>
		<div id="app"></div>
		<!-- JS -->
		<script src="{{ asset('js/app.js') }}"></script>
	</body>
</html>
