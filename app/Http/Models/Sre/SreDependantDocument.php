<?php

namespace App\Http\Models\Sre;

use Illuminate\Database\Eloquent\Model;
use App\Http\Models\Sre\SreFile;

class SreDependantDocument extends Model
{
    function file()
    {
        return $this->belongsTo(SreFile::class, 'sre_file_id');
    }
}
