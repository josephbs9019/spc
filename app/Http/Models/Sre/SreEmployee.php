<?php

namespace App\Http\Models\Sre;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Http\Models\Geo\GeoCatCountry;
use App\Http\Models\Geo\GeoCatState;
use App\Http\Models\SreCats\SreCatOffice;
use App\Http\Models\SreCats\SreCatEmployeeStatus;
use App\Http\Models\SreCats\SreCatEmployeeGender;
use App\Http\Models\SreCats\SreCatEmployeeMaritalStatus;
use App\Http\Models\Sre\SreEmployeeStudy;
use App\Http\Models\Sre\SreEmployeeIdiom;
use App\Http\Models\Sre\SreEmployeeDocument;
use App\Http\Models\Sre\SreDependant;

class SreEmployee extends Model
{
	use SoftDeletes;

	protected $fillable = [
							// Employee info
							'employeeNumber',
							'name',
							'firstName',
							'secondName',
							'jobTitle',
							'officePhone',
							'extensionPhone',
							'sre_cat_department_id',
							'departmentName',
							'sre_cat_job_id',
							'workEmail',
							'startDate',
							'endDate',
							'sre_cat_employee_status_id',
							'sre_cat_office_id',
							'sre_cat_employee_separation_type_id',
							// Personal Info
							'streetName',
							'buldingNum',
							'apartmentNum',
							'sre_cat_employee_gender_id',
							'geo_cat_country_id',
							'geo_cat_state_id',
							'neighborhood',
							'postalCode',
							'telephoneNumber',
							'mobileNumber',
							'personalEmail',
							'rfc',
							'curp',
							'passport',
							'identificationCode', // Clave de elector
							'identificationFolio', // Folio de elector
							'educationCode', // Cédula profesional
							'driverCode', // Licencia de manejo
							'armyCode', // Servicio militar
							'nss', // Número de seguro social
							'isste', // Número ISSTE
							'regimenIsste', // Régimen ISSTE
							'sre_cat_employee_marital_status_id',
							'birthDate',
							'birthCountry',
							'birthState'
	];
	// Employee info
	function office()
	{
		return $this->belongsTo(SreCatOffice::class, 'sre_cat_office_id');
	}

	function status()
	{
		return $this->belongsTo(SreCatEmployeeStatus::class, 'sre_cat_employee_status_id');
	}

	// Personal Info
	function addressCountry()
	{
		return $this->belongsTo(GeoCatCountry::class, 'geo_cat_country_id');
	}

	function addressState()
	{
		return $this->belongsTo(GeoCatState::class, 'geo_cat_state_id');
	}

	function gender()
	{
		return $this->belongsTo(SreCatEmployeeGender::class, 'sre_cat_employee_gender_id');
	}

	function maritalStatus()
	{
		return $this->belongsTo(SreCatEmployeeMaritalStatus::class, 'sre_cat_employee_marital_status_id');
	}

	function birthCountry()
	{
		return $this->belongsTo(GeoCatCountry::class, 'birthCountry');
	}

	function birthState()
	{
		return $this->belongsTo(GeoCatState::class, 'birthState');
	}

	function studies()
	{
		return $this->hasMany(SreEmployeeStudy::class);
	}

	function languages()
	{
		return $this->hasMany(SreEmployeeIdiom::class);
	}

	function idioms()
	{
			return $this->hasMany(SreEmployeeIdiom::class);
	}

	function documents()
	{
			return $this->hasMany(SreEmployeeDocument::class);
	}

	function dependants()
	{
			return $this->hasMany(SreDependant::class);
	}

}
