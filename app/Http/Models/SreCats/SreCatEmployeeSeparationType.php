<?php

namespace App\Http\Models\SreCats;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class SreCatEmployeeSeparationType extends Model
{
	use SoftDeletes;
}
