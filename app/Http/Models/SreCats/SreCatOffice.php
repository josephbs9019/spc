<?php

namespace App\Http\Models\SreCats;

use Illuminate\Database\Eloquent\Model;

class SreCatOffice extends Model
{

	function type()
	{
		return $this->belongsTo(SreCatOfficeTypes::class, 'sre_cat_office_type_id');
	}
}
