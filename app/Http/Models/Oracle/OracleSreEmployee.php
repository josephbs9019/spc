<?php

namespace App\Http\Models\Oracle;

use Illuminate\Database\Eloquent\Model;

class OracleSreEmployee extends Model
{
	protected $connection	= 'oracle';
	protected $table		= 'SIACON.VEMPLEADOS';
}
