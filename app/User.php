<?php

namespace App;

use App\Http\Models\Sre\SreEmployee;

use Illuminate\Foundation\Auth\User as Authenticatable;
use Laravel\Passport\HasApiTokens;
use Auth;

class User extends Authenticatable
{
	use HasApiTokens;

	protected $table = 'sre_users';
	protected $fillable = [
		'username',
		'isLdap',
		'password'
	];

	protected $hidden = [
		'id',
		'username',
		'password',
		'isLdap',
		'sre_employee_id',
		'remember_token',
		'created_at',
		'updated_at',
		'deleted_at'
	];

	public function getAuthPassword() {
		return $this->password;
	}

	function employee()
	{
		return $this->belongsTo(SreEmployee::class, 'sre_employee_id');
	}

}
